---
title: 'Airflow and Temperature in a Lehmann Acoustic Rack'
taxonomy:
    tag:
        - selfhosted
        - cooling
        - fans
        - server
date: '2021-04-27 00:00'
summary:
    size: 200
publish_date: '2021-04-27 00:00'
process:
    markdown: true
    twig: false
---

# Airflow and Temperature in a Lehmann Acoustic Rack

I’ve recently bought a Lehmann acoustic rack with silent fans for for my home [^1].
The main reason being that storing a server under your couch can get a bit annoying over time especially when you have to convert the couch into a bed from time to time.
In addition I wanted to reduce the noise a bit and the rack seemed like a good option.
Another benefit which I haven’t thought about before is that I have now all my electronics, besides the client devices, stored in one place.

## Dust filters for the fans

The rack sits right next to the balcony door which isn’t ideal when it comes to having a dust free environment for the servers.
I thought it would be a good idea to get dust filters for the cabinet so that I wouldn’t have to vacuum the servers all the times.
Therefore I bought magnetic filters by DEMCiflex [^2].
The first problem, altough a small one, was that they weren’t sticking to the rack on their own.
This was partially because of the protection grills in front of the fans and partially because the magnets aren’t strong enough.
Because even after I’ve removed the grills they didn’t stick very well.
When I applied the included magnet strips they remained attached to the cabinet.

## Temperature in relation to airflow

Because it was the first sunny day after I had installed the rack I wanted to check how the temperature was inside of it.
I applied the top filters without the magnet strips a while ago so I was used to seeing temperatures of around 29 degrees Celcius.
However when I applied all the fans with the magnet strips  I was quickly getting temperatures of around 33 degrees Celsius.
I first removed the top filters because I reckoned that they would block the warm air from escaping out of the case now that they were properly attached to the case.
As it turned out that was true, the temperature dropped back to 29 degrees like I was used to.
Out of curiosity I removed the bottom filter as well after around 30 minutes.
To my surprise the temperature dropped another 3 degrees to around 26 degrees Celsius.

![Temperature over time inside the cabinet](../../01.blog/2021-04-27_rack_airflow/2021-04-27_rack_temperature.png)

I really didn’t expect this to make such a huge difference especially when removing the bottom filter.
The top ones were kind of obvious and even a bit unnecessary since they blow out anyway.
Since the environment temperatures peak currently at around 20 degrees Celsius I decided to remove the filters completely.
When summer arrives with temperatures around 30 degrees it’s not acceptable to add an additional 6 degrees due to bad airflow.

So in the end I bought expensive dust filters which weren’t useful but I learned how important airflow for temperature can be in a server cabinet.
Even when the air isn’t fully blocked.


[^1]: https://www.lehmann-it.eu/en/19-inch-server-cabinets/19-inch-office-racks/19-office-rack-akustik-12-he-600x900x660-silent.html

[^2]: https://www.digitec.ch/de/s1/product/demciflex-demciflex-pc-luefter-zubehoer-280892