---
title: Home
sitemap:
    changefreq: monthly
blog_url: /blog
show_sidebar: true
show_breadcrumbs: false
show_pagination: true
content:
    items:
        - '@self.children'
    limit: 6
    order:
        by: date
        dir: desc
    pagination: true
    url_taxonomy_filters: true
feed:
    description: 'Sample Blog Description'
    limit: 10
pagination: true
---

# Notes from Andreas Zweili

### This site contains various notes which I think might be useful or interesting to someone else.
