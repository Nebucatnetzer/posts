---
routable: false
process:
    markdown: true
    twig: false
---

### Contact and License
<br>
If you want to contact me you can write an email to [contact@2li.ch](mailto:contact@2li.ch) or follow me on <a rel="me" href="https://social.linux.pizza/@nebucatnetzer">Mastodon</a>.

 <p xmlns:cc="http://creativecommons.org/ns#" xmlns:dct="http://purl.org/dc/terms/"><a property="dct:title" rel="cc:attributionURL" href="https://www.2li.ch">Notes</a> by <a rel="cc:attributionURL dct:creator" property="cc:attributionName" href="https://social.linux.pizza/@nebucatnetzer">Andreas Zweili</a> is licensed under <a href="http://creativecommons.org/licenses/by-sa/4.0/?ref=chooser-v1" target="_blank" rel="license noopener noreferrer" style="display:inline-block;">CC BY-SA 4.0<img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/sa.svg?ref=chooser-v1"></a></p>
